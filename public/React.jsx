/*///Импорт библиотек///*/

const {Formik, Field, Form} = window.Formik;
const {createSlice, configureStore} = window.RTK;
const {combineReducers} = window.Redux;
const {Provider, connect} = window.ReactRedux;
const {HashRouter, Route, Switch, withRouter} = window.ReactRouterDOM;

/*///localStorage///*/

function saveLocalStorage(values) {
  localStorage.setItem("name", values.yourName === null ? "" : values.yourName);
  localStorage.setItem("phone", values.phone === null ? "" : values.phone );
  localStorage.setItem("email", values.email === null ? "" : values.email);
  localStorage.setItem("message", values.message === null ? "" : values.message);
  localStorage.setItem("policy", values.policy);
}

/*///Открытие формы///*/

const formOpenerSlice = createSlice({
    name: 'formOpener',
    initialState: "",
    reducers: {
        requestOpen: (state, action) => { // Запрос на открытие формы и его reducer
            if (state === "")
                return action.payload;
            return state;
        },
        requestFulfilled: state => "" // Если запрос выполнен
    }
});

/*///Анимация кнопки при отправке формы///*/

const formAnimationSlice = createSlice({
    name: 'formAnimation',
    initialState: "",
    reducers: {
        formClosed: state => "",
        submitStart: state => "wait",
        submitSuccess: state => "success",
        submitError: state => "error"
    }
});

/*///Комбинирование reducer-ов///*/

const mainReducer = combineReducers({
    formOpener: formOpenerSlice.reducer,
    formAnimation: formAnimationSlice.reducer
});

const store = configureStore({reducer: mainReducer}); // Создание store-а с скомбинированным reducer-ом


/*///Форма///*/

class MainForm extends React.Component {
    constructor(props) { // Конструктор
        super(props);
        this.renderButtonText = this.renderButtonText.bind(this);
        this.step = this.step.bind(this);
    }

    componentDidMount() { // Если форма создана
        this.validateForm();
    }

    componentWillUnmount() { // Если форма удалена
        this.props.formClosed();
    }

    /*///Анимация одного кадра стрелочки///*/

    step(timestamp) {
        if (this.start === undefined) this.start = timestamp; // Сохранение времени начала анимации
        let elapsed = timestamp - this.start; // Время, прошедшее с начала анимации
        const time = 1000; // Константа = время на один оборот (1 сек)
        let element = document.querySelector('.animation-wait'); // Поиск элемента с нужной анимацией
        if (element) // Если элемент найден, то 
            element.style.setProperty('--rotateTransform', 'rotate(' + (elapsed / time * 360) + 'deg)'); // устанавливаем в css стрелочки поворот
        if (this.props.animation === "wait") { // Если анимация ожидания, то 
            window.requestAnimationFrame(this.step); // запрос некст кадра анимации поворота стрелОчки
        } else {  // Иначе
            this.start = undefined; // сбрасываем счетчик
        }
    }

/*///Рендер текста кнопки///*/

    renderButtonText() {
        if(this.props.animation === 'success') // Если форма отправена, то
            return <span className="btn-animation animation-success"> </span>; // добавляется класс и в кнопке отображается Галочка
        if(this.props.animation === 'error')  // Если ошибка отправки формы, то
            return <span className="btn-animation animation-error"> </span>; // добавляется класс и в кнопке отображается крестик
        if(this.props.animation === 'wait') { // Если форма отправляется, то
            window.requestAnimationFrame(this.step); // запрашивается кадр анимации стрелочки
            return <span className="btn-animation animation-wait"> </span>; // добавляется класс и в кнопке отображается анимация стрелочки
        }
        return <span className="btn-animation animation-none">ОТПРАВИТЬ</span>; // добавляется класс и в кнопке отображается ОТПРАВИТЬ
    }

    /*///Рендер формы///*/

    render() {
        return (
            <div>
              <Formik // Форма с полями:
                initialValues={{ yourName: localStorage.getItem("name"), email: localStorage.getItem("email"), phone: localStorage.getItem("phone"), message: localStorage.getItem("message") ,  policy: localStorage.getItem("policy") === "true" }}
                validate={values => { // Проверка полей на ошибки
                    const errors = {};
                    if (!values.yourName) {
                        errors.yourName = 'Required';Ы
                    }

                    if (!values.email) {
                        errors.email = 'Required';
                    } else if (
                        !/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/i.test(values.email)
                    ) {
                        errors.email = 'Invalid';
                    }

                    if (!values.phone) {
                        errors.phone = 'Required';
                    }

                    if (!values.policy) {
                        errors.policy = 'Required';
                    }
                    return errors; // Ошибки-объекты возвращаются
                }}
                
                onSubmit={(values, { setSubmitting }) => { // Вызывается после нажатия, если все прекрасно
                    // https://formcarry.com/s/w8nVre032a
                    this.props.submitStart(); //Запускает анимацию с начала
                    const prom = fetch( // Отправляет все на сервер
                        'https://formcarry.com/s/w8nVre032a',
                        {
                            method: 'POST',
                            mode: 'cors',
                            cache: 'no-cache',
                            credentials: 'same-origin',
                            headers: {
                                'Content-Type': 'application/json',
                                'Accept': 'application/json'
                            },
                            redirect: 'follow',
                            referrerPolicy: 'no-referrer',
                            body: JSON.stringify(values)
                        }
                    );
                    prom.then((response) => { // Если
                        if (response.ok) // все ок, то
                            this.props.submitSuccess(); // успешная отправка
                        else // Иначе
                            this.props.submitError(); // что-то пошло не так
                        setSubmitting(false);
                    })
                }}
              >
                {({ isSubmitting, handleChange, handleBlur, values, errors, validateForm}) => 
                {
                    this.validateForm = validateForm;
                    console.log(values);
                    saveLocalStorage(values);
                    return (
                  <Form>
                    <Field type="text" name="yourName" placeholder="Ваше имя" valid={errors.yourName? 'false' : 'true'}/>
                    <Field type="text" name="phone" placeholder="Телефон" valid={errors.phone? 'false' : 'true'}/>
                    <Field type="email" name="email" placeholder="E-mail" valid={errors.email? 'false' : 'true'}/>
                    <textarea
                        name="message"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.message}
                        placeholder="Ваш комментарий"
                    />
                    <label htmlFor="policy" className="c_box">
                        <Field type="checkbox" className="cb" id="policy" name="policy" checked={values.policy} />
                        <span className="cb_place"></span>
                        <div>
                            <span className="checkbox-text">
                                Отправляя заявку, я даю согласие на <a href="">обработку своих персональных данных</a>.
                            </span>
                        </div>
                    </label>
                    <button type="submit" disabled={isSubmitting || Object.keys(errors).length > 0}> 
                      {this.renderButtonText()}
                    </button>
                  </Form>
                )}}
              </Formik>
            </div>
          );
    }
}

function mapStateForm(state) { // Вытаскивает из state анимацию и кладет ее в animation
    const {formAnimation} = state;
    return {animation: formAnimation}
}

const formAnimationActions = formAnimationSlice.actions; // Вытаскиваем actions
const mapDispatchForm = {
    formClosed: formAnimationActions.formClosed,
    submitStart: formAnimationActions.submitStart,
    submitSuccess: formAnimationActions.submitSuccess,
    submitError: formAnimationActions.submitError
};
const WrappedMainForm = connect(mapStateForm, mapDispatchForm)(MainForm); // Привязываем форму к redux-у

/*///Всплывающее окно аналь-но форме///*/

class ModalWindow extends React.Component {
    constructor(props) { // History API
        super(props);
        let isOpen = props.location.pathname === "/form";
        if (isOpen) { // Если форма открыта, то в историю добавляется страница с /form
            props.history.replace("/");
            props.history.push("/form");
        }
        this.state = {
            animationInProgress: false // Анимацияя в данным момент
        }

        this.stepOpen = this.stepOpen.bind(this);
        this.playOpen = this.playOpen.bind(this);
        this.stepClose = this.stepClose.bind(this);
        this.playClose = this.playClose.bind(this);
        this.handleOffClick = this.handleOffClick.bind(this);
    }

    stepOpen(timestamp) { // Анимация всплытия окна
        if (this.startOpen === undefined) this.startOpen = timestamp;
        let elapsed = timestamp - this.startOpen;
        const time = 600;
        document.getElementById('moving-overlay').style.transform = 
            'scale(' + Math.min(elapsed / time, 1) + ')';
        if (this.id) {
            let element = document.getElementById(this.id);
            let rect = element.getBoundingClientRect();
            let centerX = (rect.left + rect.right) / 2;
            let centerY = (rect.top + rect.bottom) / 2;
            let centerString = centerX + "px " + centerY + "px";
            document.getElementById('moving-overlay').style.transformOrigin = centerString;
        }
        document.getElementById('my-fixed-overlay').style.backgroundColor =
            'rgba(20, 20, 20, ' +  Math.min(elapsed / time * 0.8, 0.8) + ')'
        if (elapsed < time) {
            window.requestAnimationFrame(this.stepOpen);
        } else {
            this.setState({animationInProgress: false});
        }
    }

    playOpen(id) { // Запускает анимацию
        if (this.state.animationInProgress) return;
        this.setState({animationInProgress: true});
        this.props.history.push("/form");
        this.startOpen = undefined;

        this.id = id;
        window.requestAnimationFrame(this.stepOpen);
    }

    stepClose(timestamp) { // Анимация утопления (окна)
        if (this.startClose === undefined) this.startClose = timestamp;
        let elapsed = timestamp - this.startClose;
        const time = 600;
        document.getElementById('moving-overlay').style.transform = 
            'scale(' + (1 - Math.min(elapsed / time, 1)) + ')';
        if (this.id) {
            let element = document.getElementById(this.id);
            let rect = element.getBoundingClientRect();
            let centerX = (rect.left + rect.right) / 2;
            let centerY = (rect.top + rect.bottom) / 2;
            let centerString = centerX + "px " + centerY + "px";
            document.getElementById('moving-overlay').style.transformOrigin = centerString;
        }
        document.getElementById('my-fixed-overlay').style.backgroundColor =
            'rgba(20, 20, 20, ' + (0.8 - Math.min(elapsed / time * 0.8, 0.8)) + ')'
        if (elapsed < time) {
            window.requestAnimationFrame(this.stepClose);
        } else {
            this.setState({animationInProgress: false});
            this.props.history.goBack();
        }
    }

    playClose() { // Запускает анимацию
        if (this.state.animationInProgress) return;
        this.setState({animationInProgress: true});
        this.startClose = undefined;
        if (this.id) {
            let element = document.getElementById(this.id);
            let rect = element.getBoundingClientRect();
            let centerX = (rect.left + rect.right) / 2;
            let centerY = (rect.top + rect.bottom) / 2;
            this.centerString = centerX + "px " + centerY + "px";
        }
        window.requestAnimationFrame(this.stepClose);
    }

    componentDidUpdate(prevProps) { // Вызывается при любом изменении state и props
        if (this.props.location !== prevProps.location)
            this.setState({animationInProgress: false});
        if (this.props.openRequest !== "") {
            this.playOpen(this.props.openRequest);
            this.props.requestFulfilled();
        }
    }

    handleOffClick(e) { // Если клик по форме, то ничего не происходит
        if (document.getElementById('my-modal').contains(e.target)) return;
        this.playClose();
    }

    /*///Рендер модального окна///*/

    render() {
        return (
            <Switch>
                <Route path="/form">
                    <div id="my-fixed-overlay">
                        <div id="moving-overlay" onClick={this.handleOffClick}> 
                            <div id="my-modal">
                                {this.props.children}
                            </div>
                        </div>
                    </div>
                </Route>
            </Switch>
        )
    }
}
/*///Привязка к redux-у///*/

function mapStateOpener(state) {
    const {formOpener} = state;
    return {openRequest: formOpener}
}

const mapDispatchOpener = {requestFulfilled: formOpenerSlice.actions.requestFulfilled};
const WrappedModalWindow = connect(mapStateOpener, mapDispatchOpener)(withRouter(ModalWindow));

function App() { // Объединение всего и вся
    return (
    <HashRouter>
        <Provider store={store}>
            <WrappedModalWindow> 
                <WrappedMainForm /> 
            </WrappedModalWindow>
        </Provider>
    </HashRouter>
    );
}

ReactDOM.render(<App />, document.getElementById('forReact')); //Кладем все к react-main

function clickHandler(e) { //Обработчик события
    e.preventDefault();
    store.dispatch(formOpenerSlice.actions.requestOpen(e.target.id));
}

document.querySelectorAll(".form-opener") // Каждой кнопке прикрепляем слушателя
    .forEach((elem) => elem.addEventListener("click", clickHandler));
